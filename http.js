import axios from "axios";

const IOKey = 'something'
const username = 'username'
const feedUrl = (feedKey) => `v2/${username}/feeds/${feedKey}/data`

const http = axios.create({
  baseURL: `https://io.adafruit.com/api/`,
  timeout: 1000,
  headers: {
    'X-AIO-Key': IOKey,
    'Content-Type': 'application/json'
  }
});

const push = function(feedKey, value) {
  new Promise((resolve, reject) => {
    http.request({
      method: 'post',
      url: feedUrl(feedKey),
      data: {
        value
      }
    }).then(res => {
      resolve(res.data)
    }).catch(error => {
      console.log(error)
      reject(error)
    })
  })
}

const getRecent = function(feedKey) {
  new Promise((resolve, reject) => {
    http.get(`${feedUrl(feedKey)}/retain`).then(res => {
      data = res.data
      resolve(data.split(',')[0])
    }).catch(error => {
      console.log(error)
      reject(error)
    })
  })
}

export { http, push, getRecent }