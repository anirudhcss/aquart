import React, { Component } from 'react'
import { View, Text } from 'react-native'

import ProgressBar from 'react-native-progress/Bar'

import defaultStyles from '../styles'

const colorMapper = {
  0: 'green',
  1: 'blue',
  2: 'yellow',
  3: 'red'
}

export default class Gauge extends Component {
  constructor(props) {
    super(props);
    this.getColor = this.getColor.bind(this)
  }

  getColor() {
    let percent = this.props.gauge * 100
    return colorMapper[Math.floor(percent/25)] || 'red'
  }

  render() {
    return(
      <View style={defaultStyles.view}>
        <Text style={defaultStyles.label}>
          Gauge
        </Text>
        <ProgressBar progress={this.props.gauge} width={230} height={20} color={this.getColor()}/>
        <Text style={{textAlign: 'center'}}>{this.props.gauge * 100}%</Text>
      </View>
    )
  }
}