import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';

import defaultStyles from "../styles";

export default class DisplayBox extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style = {defaultStyles.view}>
        <Text style = {defaultStyles.label}>
          {this.props.label}
        </Text>
        <Text style = {styles.value}>
          {this.props.value || '-'} {this.props.units}
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  value: {
    textAlign: "center",
    fontSize: 20,
    color: "rgb(94, 148, 78)"
  }
})
