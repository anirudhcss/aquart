import React, { Component } from 'react';
import { StyleSheet, Text, View, TextInput } from 'react-native';

import defaultStyles from "../styles";

export default class Threshold extends Component {
  constructor(props) {
    super(props);
    this.state = { threshold: '1000' };
    this.onThresholdChange = this.onThresholdChange.bind(this)
  }

  onThresholdChange(threshold) {
    this.props.onThresholdChange(threshold)
  }

  render() {
    return (
      <View style = {defaultStyles.view}>
        <Text style = {defaultStyles.label}>
          Threshold
        </Text>
        <TextInput
          style = {styles.input}
          placeholder = "Type Your Threshold value here!"
          keyboardType = "number-pad"
          value = {this.props.threshold}
          onChangeText = {this.onThresholdChange}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  input: {
    padding: 5,
    borderRadius: 4,
    borderWidth: 1,
    borderColor: '#d6d7da',
    backgroundColor: '#EAEAEA'
  }
})
