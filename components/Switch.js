import React, { Component } from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
// toggle-switch-off-outline -> off
// toggle-switch -> on


import defaultStyles from "../styles";

export default class Switch extends Component {
  constructor(props) {
    super(props)

    this.switchPressed = this.switchPressed.bind(this)
  }

  switchPressed() {
    this.props.onSwitchChange()
  }

  render() {
    return (
      <View style={defaultStyles.view}>
        <Text style={defaultStyles.label}>
          On/Off Switch
        </Text>

        <Button
          title={this.props.switch ? 'Switch Off' : 'Switch On'}
          style={defaultStyles.button}
          onPress={this.switchPressed}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  checkbox: {
    padding: 5,
    borderRadius: 4,
    borderWidth: 1,
    borderColor: '#d6d7da',
    backgroundColor: '#EAEAEA'
  }
})