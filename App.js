import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';

import Switch from "./components/Switch";
import Threshold from "./components/Threshold";
import Gauge from "./components/Gauge";
import DisplayBox from "./components/DisplayBox";

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = { switch: true, threshold: '1000', currentFlow: null, totalOutput: null, gauge: 0.79 }

    this.toggleSwitch = this.toggleSwitch.bind(this)
    this.changeThreshold = this.changeThreshold.bind(this)
  }

  toggleSwitch() {
    this.setState(state => ({
      switch: !state.switch
    }))
    // Push to the server
  }

  changeThreshold(threshold) {
    this.setState({threshold})
    // Push to the server
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style = {styles.heading}>Aquart</Text>
        <Switch switch={this.state.switch} onSwitchChange={this.toggleSwitch}/>
        {this.state.switch ?
          <View>
            <Threshold threshold={this.state.threshold} onThresholdChange={this.changeThreshold}/>
            <Gauge gauge={this.state.gauge} />
            <DisplayBox
              label="current flow rate"
              value={this.state.currentFlow}
              units="liters/sec"
            />
            <DisplayBox
              label="total output"
              value={this.state.totalOutput}
              units="liters"
            />
          </View> : <Text style={
            {
              ...styles.heading,
              fontSize: 18
            }
          }>Switch is turned OFF</Text>
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  heading: {
    fontSize: 24,
    fontWeight: 'bold',
    margin: 20
  },
});
