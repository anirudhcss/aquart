import { StyleSheet } from 'react-native';

export default defaultStyles = StyleSheet.create({
  view: {
    padding: 10,
    borderRadius: 4,
    borderWidth: 1,
    borderColor: '#d6d7da',
    width: 250,
    minHeight: 75,
    margin: 10
  },
  label: {
    paddingBottom: 5,
    textTransform: 'uppercase',
    textAlign: 'center'
  },
  button: {
    backgroundColor: 'rgb(94, 148, 78)'
  }
})
